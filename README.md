# geolocation-tracker

This app is intended to run via Express server. Furthermore Express server additionaly can be used also to serve API. Good read on this is - https://dennisreimann.de/articles/vue-cli-serve-express.html

Uses Heroku, which is connected via bitbucket pipelines.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development. Compiles and starts to serve for production
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
