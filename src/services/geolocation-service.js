class Geolocation {
  constructor() {
    this.supported = null;
    this.options = {
      enableHighAccuracy: true,
      maximumAge: 30000,
      timeout: 27000
    };

    this.watchId = null;
    this.watchStack = [];

    this.init();
  }

  init() {
    if (!navigator.geolocation) {
      this.supported = false;
    } else {
      this.supported = true;
    }
  }

  isSupported() {
    return this.supported;
  }

  getCurrentPosition() {
    return new Promise((resolve, reject) => {
      if (!this.supported) {
        reject(false);
      }

      this._getCurrentPosition()
        .then(result => {
          resolve(result);
        })
        .catch(error => {
          reject(error);
        });
    });
  }

  _getCurrentPosition() {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(
        position => {
          resolve(position);
        },
        error => {
          reject(error);
        },
        this.options
      );
    });
  }

  watchPosition() {
    this.watchId = navigator.geolocation.watchPosition(
      this._watchSuccess,
      this._watchError,
      this.options
    );
    return this.watchStack;
  }

  _watchSuccess(position) {
    this.watchStack.push({
      success: true,
      position: position,
      error: null
    });
  }

  _watchError(error) {
    this.watchStack.push({
      success: false,
      position: null,
      error: error
    });
  }

  clearWatch() {
    navigator.geolocation.clearWatch(this.watchId);
    this.watchId = null;
    this.watchStack = Array.prototype; // previous values not saved, hopefully what's returned in watchPosition is available in memory
  }
}

export default new Geolocation();
