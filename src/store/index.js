import Vue from "vue";
import Vuex from "vuex";
import geolocation from "./modules/geolocation";
import map from "./modules/map";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    geolocation,
    map
  }
});
