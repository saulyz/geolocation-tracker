/* eslint no-console: ["error", { allow: ["warn", "error"] }] */

const GEOPOSITIONING_OPTIONS = {
  enableHighAccuracy: true,
  maximumAge: 30000,
  timeout: 27000
};

export default {
  namespaced: true,

  state: {
    userLocationStack: [],
    userMovementHeading: null,
    userMovementSpeed: null,
    userMovementAccuracy: null,
    isWatching: false,
    watchId: null
  },

  getters: {
    userLocationEntriesCount: state => {
      return state.userLocationStack.length;
    },

    userLocation: state => {
      let lastIndex = state.userLocationStack.length;
      // eslint-disable-next-line no-console
      console.log('position - ', state.userLocationStack[lastIndex - 1]);
      return state.userLocationStack[lastIndex - 1];
    },

    userMovementSpeed: state => {
      return state.userMovementSpeed;
    },

    userMovementHeading: state => {
      return state.userMovementHeading;
    }
  },

  mutations: {
    addUserLocation(state, GeolocationPosition) {
      state.userLocationStack.push(GeolocationPosition.coords);
    },

    clearUserLocation(state) {
      state.userLocationStack = [];
    },

    setUserMovementInfo(state, GeolocationPosition) {
      let heading = GeolocationPosition.coords.heading;
      let speed = GeolocationPosition.coords.speed;
      let accuracy = GeolocationPosition.coords.accuracy;
      if (heading) {
        state.userMovementHeading = parseInt(heading); // in degrees
      }
      if (speed) {
        state.userMovementSpeed = parseInt(speed); // in meters per second
      }
      if (accuracy) {
        state.userMovementAccuracy = parseInt(accuracy); // lat, lng props expressed in meters per second
      }
    },

    clearUserMovementInfo(state) {
      state.userMovementHeading = state.userMovementSpeed = state.userMovementAccuracy = null;
    },

    startWatch(state) {
      state.isWatching = true;
    },

    stopWatch(state) {
      state.isWatching = false;
    },

    toggleWatch(state) {
      state.isWatching = !state.isWatching;
    },

    setWatchId(state, id) {
      state.watchId = id;
    }
  },

  actions: {
    getCurrentPosition({ commit }) {
      navigator.geolocation.getCurrentPosition(
        position => {
          // sucess
          commit("addUserLocation", position);
          commit("setUserMovementInfo", position);
        },
        error => {
          // error
          console.error("geopositioning error occured:", error);
        },
        GEOPOSITIONING_OPTIONS
      );
    },

    startWatchPosition({ commit }) {
      commit("startWatch");

      let watchId = navigator.geolocation.watchPosition(
        position => {
          // sucess
          commit("setWatchId", watchId);
          commit("addUserLocation", position);
          commit("setUserMovementInfo", position);
        },
        () => {
          // error
          commit("setWatchId", watchId);
        },
        GEOPOSITIONING_OPTIONS
      );
    },

    stopWatchPosition({ commit, state }) {
      commit("stopWatch");
      navigator.geolocation.clearWatch(state.watchId);
      commit("clearUserMovementInfo");
    },

    toggleWatchPosition({ dispatch, state }) {
      if (state.isWatching) {
        dispatch("stopWatchPosition");
      } else {
        dispatch("startWatchPosition");
      }
      let zoomLevel = state.isWatching ? "zoomIn" : "zoomOut";
      dispatch("map/adjustZoomLevel", zoomLevel, { root: true });
    }
  }
};
