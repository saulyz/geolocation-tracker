const ZOOM_LEVEL_DEFAULT = 14;

export default {
  namespaced: true,

  state: {
    center: { lat: 54.678388, lng: 25.287107 },
    zoomLevel: ZOOM_LEVEL_DEFAULT
  },

  getters: {},

  mutations: {
    setCenter(state, coordinates) {
      state.center = coordinates;
    },

    setZoomLevel(state, level) {
      if ("zoomIn" == level) {
        state.zoomLevel = 19;
      }
      if ("zoomOut" == level) {
        state.zoomLevel = ZOOM_LEVEL_DEFAULT;
      }
    }
  },

  actions: {
    adjustZoomLevel({ commit }, zoomLevel) {
      commit("setZoomLevel", zoomLevel);
    }
  }
};
